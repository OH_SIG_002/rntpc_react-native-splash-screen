# v3.3.1-rc.1
## 更新内容
* chore: remove code of Android and iOS 由 @qinjianqili 贡献 [9fbd0b3ce8b977e09b5e2bf2951cfb2bb742cd3a](https://gitee.com/openharmony-sig/rntpc_react-native-splash-screen/pulls/1)
* feat: add HarmonyOS support for react-native-splash-screen 由 @qinjianqili 贡献 [5497f6e44db3865ab6556ab0a99afb507f2331a4](https://gitee.com/openharmony-sig/rntpc_react-native-splash-screen/pulls/2)
* refactor: Call static variable with class name and Modifying the Copyright Header 由 @qinjianqili 贡献 [5ff4be51cf1374bab4b467dd1dc19c2dda132832](https://gitee.com/openharmony-sig/rntpc_react-native-splash-screen/pulls/3)
* 添加README.OpenSource和COMMITTERS.md文件 由 @qinjianqili 贡献 [598eed2fb0fc217e7eb1d63ea0a9fba75b0914b9](https://gitee.com/openharmony-sig/rntpc_react-native-splash-screen/pulls/4)
* pre-release: @react-native-ohos/react-native-splash-screen@3.3.1-rc.1 由 @qinjianqili 贡献 [b1a2835fd8224cf98535f3bf7be68ce127cdd634](https://gitee.com/openharmony-sig/rntpc_react-native-splash-screen/pulls/5)