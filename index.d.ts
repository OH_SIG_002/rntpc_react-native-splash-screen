// Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import { Platform } from "react-native";

declare module "react-native-splash-screen" {
    class SplashScreenCommon {
        static hide(): void;
        static show(): void;
    }

    class SplashScreenHarmony {
        hide(): void;
        show(): void;
    }

    export default class SplashScreen extents ((Platform.OS === 'ios' || Platform === 'android') ? SplashScreenCommon : SplashScreenHarmony) {

    }
}
