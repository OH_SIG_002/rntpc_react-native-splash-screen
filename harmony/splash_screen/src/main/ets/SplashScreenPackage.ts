// Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import { RNPackage, TurboModuleContext, TurboModulesFactory } from '@rnoh/react-native-openharmony/ts';
import type { TurboModule } from '@rnoh/react-native-openharmony/ts';
import { SplashScreen } from './SplashScreen';

class SplashScreenTurboModulesFactory extends TurboModulesFactory {
  createTurboModule(name: string): TurboModule | null {
    if (name === SplashScreen.NAME) {
      return new SplashScreen(this.ctx);
    }
    return null;
  }

  hasTurboModule(name: string): boolean {
    return name === SplashScreen.NAME;
  }
}

export class SplashScreenPackage extends RNPackage {
  createTurboModulesFactory(ctx: TurboModuleContext): TurboModulesFactory {
    return new SplashScreenTurboModulesFactory(ctx);
  }
}
